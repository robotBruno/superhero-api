
*** Settings ***
Documentation      Consultar herói

Library            Collections
Library            RequestsLibrary

*** Variables ***
${base_api_url}    https://superheroapi.com/api/2803271363239685

*** Test Cases ***
Consultar a ficha de um super herói (/id)
    Dado que esteja conectado na SuperHeroAPI
    Quando requisitar o histórico do super herói "Green Arrow"
    Então deve ser retornado que a sua inteligência é superior a "80"
    E deve ser retornado que o seu verdadeiro nome é ser "Oliver Queen"
    E deve ser retornado que é afiliado do grupo "Justice League Elite"

Consultar qual o super herói mais inteligente, rápido e forte (/id/powerstats)
    Dado que esteja conectado na SuperHeroAPI
    Quando requisitar as estatísticas de poder dos super heróis "Flash" e "Ant-Man"
    Então deve ser retornado que o mais inteligente é o herói "Ant-Man"
    E deve ser retornado que o mais rápido é o herói "Flash"
    E deve ser retornado que o mais forte é o herói "Ant-Man"

*** Keywords ***
#Primeiro cenario
Dado que esteja conectado na SuperHeroAPI

    Create Session      heroi-api       ${base_api_url}

Quando requisitar o histórico do super herói "${heroi}"

    ${INFO_HERO}        Get Request     heroi-api                      uri=/298
    Should Be Equal As Strings          ${INFO_HERO.status_code}       200
    Set Test Variable                   ${INFO_HERO}                   ${INFO_HERO.json()}

Então deve ser retornado que a sua inteligência é superior a "${inteligencia}"

    Log                       ${INFO_HERO['powerstats']['intelligence']}
    Log                       ${inteligencia}
    ${VERDADE} =              Set Variable             A sua inteligência é maior que 80
    ${FALSO} =                Set Variable             A sua inteligência é menor que 80
    ${RESULTADO} =            Set variable if          ${INFO_HERO['powerstats']['intelligence']}>${inteligencia}    ${VERDADE}      ${FALSO}
    Log                       ${RESULTADO}

E deve ser retornado que o seu verdadeiro nome é ser "${full_name}"

    Dictionary Should Contain Item         ${INFO_HERO['biography']}      full-name        ${full_name}

E deve ser retornado que é afiliado do grupo "${afiliado}"

    List Should Contain Value              ${INFO_HERO['connections']}     group-affiliation       ${afiliado}

# Segundo Cenario
Quando requisitar as estatísticas de poder dos super heróis "Flash" e "Ant-Man"
    #Flash
    ${FLASH}        Get Request         heroi-api                         uri=/263            
    Should Be Equal As Strings          ${FLASH.status_code}              200
    Log                                 ${FLASH.json()['powerstats']}
    Set Test Variable                   ${FLASH_PODERES}                  ${FLASH.json()['powerstats']}

    #Ant-man
    ${ANT_MAN}      Get Request         heroi-api                         uri=/30
    Should Be Equal As Strings          ${ANT_MAN.status_code}            200
    Log                                 ${ANT_MAN.json()['powerstats']}
    Set Test Variable                   ${ANT_MAN_PODERES}                ${ANT_MAN.json()['powerstats']}


Então deve ser retornado que o mais inteligente é o herói "Ant-Man"

    Log                  ${FLASH_PODERES['intelligence']}
    Log                  ${ANT_MAN_PODERES['intelligence']}
    ${VERDADE} =         Set Variable                               Flash é mais inteligente que o Ant-man
    ${FALSO} =           Set Variable                               Ant-Man é mais inteligente que o Flash
    ${RESULTADO} =       Set variable if                            ${FLASH_PODERES['intelligence']}>${ANT_MAN_PODERES['intelligence']}  ${VERDADE}  ${FALSO}
    Log                  ${RESULTADO}

E deve ser retornado que o mais rápido é o herói "Flash"
    Log                  ${FLASH_PODERES['speed']}
    Log                  ${ANT_MAN_PODERES['speed']}
    ${VERDADE} =         Set Variable                               Flash é mais rápido que o Ant-Man        
    ${FALSO} =           Set Variable                               Ant-Man é mais rápido que o o Flash
    ${RESULTADO} =       Set variable if                            ${FLASH_PODERES['speed']}>${ANT_MAN_PODERES['speed']}  ${VERDADE}  ${FALSO}
    Log                  ${RESULTADO}

E deve ser retornado que o mais forte é o herói "Ant-Man"
    Log                  ${FLASH_PODERES['strength']}
    Log                  ${ANT_MAN_PODERES['strength']}
    ${VERDADE} =         Set Variable                               Flash é mais forte que o Ant-Man        
    ${FALSO} =           Set Variable                               Ant-Man é mais forte que o o Flash
    ${RESULTADO} =       Set variable if                            ${FLASH_PODERES['strength']}>${ANT_MAN_PODERES['strength']}  ${VERDADE}  ${FALSO}
    Log                  ${RESULTADO}